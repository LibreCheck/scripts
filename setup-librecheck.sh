#!/bin/bash
echo "This will set up the LibreCheck check-in software. You must have git and php (and tar, but it should already be installed on your distribution) installed. This script is designed for GNU/Linux systems."
#read -n 1 -s -r -p "Press any key to continue"

latestVersion=`curl -s 'https://api.github.com/repos/LibreCheck/prototype/releases/latest' | ./jq -r '.tag_name'`
echo $latestVersion

# Add startup scripts
if [ ! -f ~/LibreCheck/version ]; then
    echo "bash ~/LibreCheck/scripts/startup.sh" >> ~/.profile
fi

rm -rf ~/LibreCheck
mkdir ~/LibreCheck
cd ~/LibreCheck

wget "https://github.com/LibreCheck/prototype/releases/download/$latestVersion/librecheck-$latestVersion-x86_64.AppImage"
wget "https://github.com/LibreCheck/prototype/releases/download/$latestVersion/php-backend-$latestVersion.tar.gz"
chmod +x "librecheck-$latestVersion-x86_64.AppImage"

echo $latestVersion > version

mkdir backend
cd backend
tar -pxvzf "../php-backend-$latestVersion.tar.gz"

cd ..
git clone https://gitlab.com/LibreCheck/scripts
cd scripts
git pull
cd ..

#read -p "Do you want to set up an automatic updater? [y\n]" answer


#read -p "Do you want to add a shortcut to easily start librecheck? (doesn't work 100% of the time) [y\n]" answer
#if [ $answer == "y" ]; then
	mkdir ~/.local/share/applications
	cp scripts/librecheck.desktop ~/.local/share/applications

#fi