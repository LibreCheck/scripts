#!/bin/bash
# This script is the one that runs when the user logs into the computer. It runs the updater.

# Pulls the latest copy of the maintainence scripts
cd ~/LibreCheck/scripts
git pull

latestVersion=`curl -s 'https://api.github.com/repos/LibreCheck/prototype/releases/latest' | ./jq -r '.tag_name'`
echo $latestVersion

localVersion=`cat ~/LibreCheck/version`
echo $localVersion

if [ ! $localVersion == $latestVersion ]; then
    echo "Updating"
    bash ~/LibreCheck/scripts/setup-librecheck.sh
fi