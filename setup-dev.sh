mkdir ~/.librecheck
mkdir ~/.librecheck/config
mkdir ~/.librecheck/logs

printf "groupid,personid,first,last,guardname,dob,gender,medical\n2345678901,1,Ted,Doe,John Doe,2007-03-17,male,none\n2345678901,2,Amy,Doe,John Doe,2010-04-09,female,none\n3345678901,3,Other,Person,Some Person,2003-03-12,male,none\n3345678901,4,Other,Guy,Some Person,2003-03-12,male,none" > ~/.librecheck/config/people.csv

printf "Test place\nAnother place" > ~/.librecheck/config/places.txt

printf "The following are the ID's in people.csv of people who are checked into the system at the moment:\n" > ~/.librecheck/config/checked.txt

echo "Sucessfully set configuration files to defaults."