# LibreCheck Scripts
These are simple scripts for installing and managing LibreCheck on GNU/Linux systems.

To install use `./setup-librecheck.sh`

To setup configuration files use `./setup-dev.sh`